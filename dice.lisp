#! sbcl --script

(defun lines-from (filename)
  "extracts all lines from a file into a list consisting of each line"
  (with-open-file (stream filename)
    ;; reads the lines from a file, yielding nil when EOF is hit
    (loop for line = (read-line stream nil nil)
	  ;; while we are receiving valid lines 
	  while line
	  ;; collect 
	  collect line)))

(defun string-split (char string)
  "splits a string into a list, containing the string before and after a character"
  (let ((p (position char string)))
    (if p
	(list (subseq string 0 p) (subseq string (1+ p)))
	(list string nil))))

(defvar *words* (make-hash-table  :test #'equal))
(defvar *word-file* "dicewords.txt")
;;; using shell arguments is a terrible fucking idea for a security application
;;; lmfao

(defun load-hash-table (pairs table)
  "loads a hash table from a list of key-value pairs"
  (loop for pair in pairs
	do (setf (gethash (first pair) table)
		 (second pair))))

(load-hash-table
 (mapcar (lambda (str) (string-split #\tab str)) (lines-from *word-file*))
 *words*)

(defun fetch-word (dice)
  "fetches the word corresponding to a five-digit dice roll"
  (gethash dice *words*))

(defun into-words (dice-rolls)
  "turns a list of 5-digit dice-rolls into a list of words"
  (loop for roll in dice-rolls
	collect (fetch-word roll)))

(defun dice-char-p (character)
  "verifies that a character is within the inclusive range 1 - 6"
  (let ((digit (digit-char-p character)))
    (if digit 
	(and (>= digit 1) (<= digit 6)))))

(defun valid-input-p (in-string)
  "verifies that an input is a five-digit dice-roll"
  (and (= (length in-string) 5)
       (every #'dice-char-p
	      (coerce in-string 'list))))

(defun read-dice ()
  "collects dice from the command line, one dice roll at a time"
  (print "please enter your 5-digit dice rolls one line at a time")
  (print "when you are finished, hit enter")
  (fresh-line)
  (loop for dice = (read-line)
	unless (equal dice "")
	  collect dice
	until (equal dice "")))

(defun printlist (lst)
  (format t "~{~a~^ ~}" lst))

(let* ((lst (read-dice))
       (validated (remove-if-not #'valid-input-p lst)))
  (format t "using validated list of input ~{~a~^ ~}~%" validated)
  (printlist (into-words validated)))

(fresh-line)


