A simple commandline utility to convert a list of 5-digit dice rolls into a diceware password, using EFF's 2016 diceware password list.

# Usage

removed command line execution because of the obvious security risk of terminal history saving your diceware input
to run it, use
```
./dice.lisp
```
and follow the instructions given by the program

sinple as.

if you want to use a different diceware wordlist, then replace dicewords.txt with whatever you want

# Notice

This is written in SBCL common lisp. At the current time it doesn't rely on anything implementation-specific
